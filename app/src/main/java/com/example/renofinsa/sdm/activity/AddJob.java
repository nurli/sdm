package com.example.renofinsa.sdm.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.model.Value;
import com.example.renofinsa.sdm.network.MyServices;
import com.example.renofinsa.sdm.network.ServiceGenerator;
import com.example.renofinsa.sdm.util.Config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddJob extends AppCompatActivity {

    ImageView gambar, back, ok;
    EditText tglBerakhir, jamMulai, jamSelesai, namaJob, posisi, deskripsi, lokasi;
    protected static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    private AlertDialog mAlertDialog;
    String imagePath;
    private int mYear, mMonth, mDay, mHour, mMinute, mSecond;
    TimePickerDialog timeDialog;
    DatePickerDialog dialog;
    Uri uri;
    SharedPreferences sharedPreferences;
    String sIDUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_job);

        tglBerakhir = findViewById(R.id.etTglBerakhir);
        jamMulai = findViewById(R.id.etJamMulai);
        jamSelesai = findViewById(R.id.etJamBerakhir);
        gambar = findViewById(R.id.ivGambar);
        back = findViewById(R.id.ivBack);
        ok = findViewById(R.id.ivOk);
        namaJob = findViewById(R.id.etNamaJob);
        posisi = findViewById(R.id.etPosisi);
        lokasi = findViewById(R.id.etLokasi);
        deskripsi = findViewById(R.id.etDeskripsi);

        sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        sIDUsers = sharedPreferences.getString(Config.ID, "");

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(namaJob.getText().toString())){
                    namaJob.setError("Masukan Nama Job");
                }else if (TextUtils.isEmpty(posisi.getText().toString())){
                    posisi.setError("Masukan Posisi Job");
                }else if (TextUtils.isEmpty(lokasi.getText().toString())){
                    lokasi.setError("Masukan Lokasi Job");
                }else if (TextUtils.isEmpty(tglBerakhir.getText().toString())){
                    tglBerakhir.setError("Masukan Tgl Akhir Job");
                }else if (TextUtils.isEmpty(jamMulai.getText().toString())){
                    jamMulai.setError("Masukan Jam Mulai Pekerjaan");
                }else if (TextUtils.isEmpty(jamSelesai.getText().toString())){
                    jamSelesai.setError("Masukan Jam Akhir Pekerjaan");
                }else if (TextUtils.isEmpty(deskripsi.getText().toString())){
                    deskripsi.setError("Masukan Deskripsi Job");
                }else {
                    simpanJob();
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tglBerakhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                dialog = new DatePickerDialog(AddJob.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        tglBerakhir.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    }
                }, mYear, mMonth, mDay);
                dialog.show();
            }
        });
        jamMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                mSecond = c.get(Calendar.SECOND);
                timeDialog = new TimePickerDialog(AddJob.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        jamMulai.setText(hourOfDay + ":"+minute);
                    }
                }, mHour, mMinute, false);
                timeDialog.show();
            }
        });
        jamSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                mSecond = c.get(Calendar.SECOND);
                timeDialog = new TimePickerDialog(AddJob.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        jamSelesai.setText(hourOfDay + ":"+minute);
                    }
                }, mHour, mMinute, false);
                timeDialog.show();
            }
        });




        gambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickFormGallery();
            }
        });

    }

    private void simpanJob() {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(AddJob.this);
        progressDialog.setTitle("Loading...");
        progressDialog.show();
        File gambar = new File(imagePath);
        File compressImageFile = null;
        try {
            compressImageFile = new Compressor(this).compressToFile(gambar);
        } catch (IOException e) {
            e.printStackTrace();
        }

        MyServices myServices = ServiceGenerator.createService(MyServices.class);
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), compressImageFile);

        MultipartBody.Part Gambar = MultipartBody.Part.createFormData("uploaded_gambar", gambar.getName(), requestBody);
        RequestBody idUsers = RequestBody.create(MediaType.parse("text/plain"), sIDUsers);
        RequestBody NamaJob = RequestBody.create(MediaType.parse("text/plain"), namaJob.getText().toString());
        RequestBody Posisi = RequestBody.create(MediaType.parse("text/plain"), posisi.getText().toString());
        RequestBody Lokasi = RequestBody.create(MediaType.parse("text/plain"), lokasi.getText().toString());
        RequestBody Deskripsi = RequestBody.create(MediaType.parse("text/plain"), deskripsi.getText().toString());
        RequestBody TglBerakhir = RequestBody.create(MediaType.parse("text/plain"), tglBerakhir.getText().toString());
        RequestBody JamMulai = RequestBody.create(MediaType.parse("text/plain"), jamMulai.getText().toString());
        RequestBody JamAkhir = RequestBody.create(MediaType.parse("text/plain"), jamSelesai.getText().toString());

        Call<Value> call = myServices
                .tambahJob(Gambar,idUsers,NamaJob, Posisi, Lokasi, Deskripsi,TglBerakhir, JamMulai, JamAkhir);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                progressDialog.dismiss();
                Toast.makeText(AddJob.this, "Sukses", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(AddJob.this, HalamanAdmin.class));
                kosong();
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(AddJob.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                kosong();
            }
        });
    }

    private void kosong() {
        namaJob.setText("");
        namaJob.setText("");
        posisi.setText("");
        lokasi.setText("");
        deskripsi.setText("");
        tglBerakhir.setText("");
        jamMulai.setText("");
        jamSelesai.setText("");

    }

    private void pickFormGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(AddJob.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                    getString(R.string.permission_read_storage_rationale),
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT)
                    .setType("image/*")
                    .setAction(Intent.ACTION_PICK);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                String[] mimeTypes = {"image/jpeg", "image/png"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }

            final Intent chooserIntent = Intent.createChooser(intent, "Choose Image");
            startActivityForResult(chooserIntent, 1010);
        }
    }
    protected void requestPermission(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(AddJob.this, permission)) {
            showAlertDialog(getString(R.string.permission_title_rationale), rationale,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(AddJob.this,
                                    new String[]{permission}, requestCode);
                        }
                    }, getString(R.string.label_ok), null, getString(R.string.label_cancel));
        } else {
            ActivityCompat.requestPermissions( AddJob.this, new String[]{permission}, requestCode);
        }
    }

    protected void showAlertDialog(@Nullable String title, @Nullable String message,
                                   @Nullable DialogInterface.OnClickListener onPositiveButtonClickListener,
                                   @NonNull String positiveText,
                                   @Nullable DialogInterface.OnClickListener onNegativeButtonClickListener,
                                   @NonNull String negativeText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddJob.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positiveText, onPositiveButtonClickListener);
        builder.setNegativeButton(negativeText, onNegativeButtonClickListener);
        mAlertDialog = builder.show();
    }
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == RESULT_OK && requestCode == 1010){
            Uri imageUri = intent.getData();
            try {
                InputStream imageStream = getContentResolver().openInputStream(imageUri);
                Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                selectedImage = getResizedBitmap(selectedImage, 1024);// 400 is for example, replace with desired size

                gambar.setImageBitmap(selectedImage);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(imageUri, filePathColumn, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imagePath = cursor.getString(columnIndex);


            cursor.close();
            gambar.setVisibility(View.VISIBLE);

        } else {

            gambar.setVisibility(View.GONE);
            Toast.makeText(AddJob.this, "Try again with another image", Toast.LENGTH_SHORT).show();
        }
    }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_ok, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case R.id.ok:
//                Toast.makeText(this, "Sukses", Toast.LENGTH_SHORT).show();
//                onBackPressed();
//                break;
//            case android.R.id.home:
//                onBackPressed();
//                supportFinishAfterTransition();
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }
}

package com.example.renofinsa.sdm.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.model.Value;
import com.example.renofinsa.sdm.network.MyServices;
import com.example.renofinsa.sdm.network.ServiceGenerator;

import retrofit2.Callback;
import retrofit2.Response;

public class Daftar extends AppCompatActivity {

    ProgressBar pbdaftar;
    TextView tvLogin;
    Button btnDaftar;
    ProgressBar progressBar;
    String level;
    EditText namaLengkap, email, pass, rePass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);


        pbdaftar = findViewById(R.id.pbDaftar);
        tvLogin = findViewById(R.id.tvLoginIn);
        btnDaftar = findViewById(R.id.btnDaftar);
        namaLengkap = findViewById(R.id.etNamaLengkap);
        email = findViewById(R.id.etEmail);
        pass = findViewById(R.id.etPassword);
        rePass = findViewById(R.id.etRePassword);



        pbdaftar.setVisibility(View.GONE);

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Daftar.this, Login.class));
            }
        });

        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(namaLengkap.getText().toString())){
                    namaLengkap.setError("Masukan Email Anda");
                }else if (TextUtils.isEmpty(email.getText().toString())){
                    email.setError("Masukan Email Anda");
                }else if (TextUtils.isEmpty(pass.getText().toString())){
                    pass.setError("Masukan Password Anda");
                }else if (TextUtils.isEmpty(rePass.getText().toString())){
                    rePass.setError("Anda perli verifikasi Password");
                }else {
                    if (!pass.getText().toString().equals(rePass.getText().toString())){
                        pass.setError("Password Anda dengan password verifikasi tidak sama !");
                    }else {
                        simpanPendaftaran();
                    }
                }
            }
        });
    }

    private void simpanPendaftaran() {
        retrofit2.Call<Value> call = ServiceGenerator.createService(MyServices.class)
                .daftarBMS(namaLengkap.getText().toString(),
                        email.getText().toString(),
                        pass.getText().toString());
        pbdaftar.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(retrofit2.Call<Value> call, Response<Value> response) {
                String value = response.body().value;
                String message = response.body().message;
                if (value.equals("1")){
                    pbdaftar.setVisibility(View.GONE);
                    Toast.makeText(Daftar.this, message, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Daftar.this, Login.class));
                    kosong();
                }else {
                    pbdaftar.setVisibility(View.GONE);
                    Toast.makeText(Daftar.this, message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<Value> call, Throwable t) {
                pbdaftar.setVisibility(View.GONE);
                Toast.makeText(Daftar.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void kosong() {
        namaLengkap.setText("");
        email.setText("");
        pass.setText("");
        rePass.setText("");
    }
}

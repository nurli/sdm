package com.example.renofinsa.sdm.activity;

import android.Manifest;
import android.app.Instrumentation;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.fragment.Job;
import com.example.renofinsa.sdm.model.Value;
import com.example.renofinsa.sdm.network.MyServices;
import com.example.renofinsa.sdm.network.ServiceGenerator;
import com.example.renofinsa.sdm.util.PicassoCache;
import com.google.gson.GsonBuilder;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.renofinsa.sdm.activity.AddJob.REQUEST_STORAGE_READ_ACCESS_PERMISSION;

public class DetailJob extends AppCompatActivity {

    Button btnEdit, btnHapus;
    public String status;
    ImageView map, gambar,share, back;
    EditText namaJob, posisi,tglBerakhir, lokasi, jamMulai, jamAkhir, deskripsi;
    com.example.renofinsa.sdm.model.Job job;
    private AlertDialog mAlertDialog;
    String imagePath,sIDUsers, sIdJob ;
    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_job);

        btnEdit = findViewById(R.id.btnEdit);
        btnHapus = findViewById(R.id.btnHapus);
        map = findViewById(R.id.ivMaps);
        gambar = findViewById(R.id.ivGambar);
        namaJob = findViewById(R.id.etJob);
        posisi = findViewById(R.id.etPosisi);
        tglBerakhir = findViewById(R.id.etTglAkhir);
        lokasi = findViewById(R.id.etLokasi);
        jamAkhir = findViewById(R.id.etJamAkhir);
        jamMulai = findViewById(R.id.etJamMulai);
        deskripsi = findViewById(R.id.etDeskripsi);
        back = findViewById(R.id.ivBack);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        job = new GsonBuilder().create().fromJson(getIntent().getStringExtra("job"), com.example.renofinsa.sdm.model.Job.class);

        namaJob.setText(job.nama_job);
        posisi.setText(job.posisi);
        tglBerakhir.setText(job.tgl_berakhir);
        lokasi.setText(job.lokasi);
        jamMulai.setText(job.jam_mulai);
        jamAkhir.setText(job.jam_berakhir);
        deskripsi.setText(job.deskripsi);
        //gambar.setImageResource(Integer.parseInt(job.uploaded_gambar));
        sIDUsers = job.id_users;
        sIdJob = job.id_job;

        gambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickFormGallery();
            }
        });

        PicassoCache.getPicassoInstance(DetailJob.this)
                .load("https://anurli.com/image/photo/" + job.uploaded_gambar)
                .into(gambar);

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailJob.this, Map.class));
            }
        });



        btnEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ubahJob();
            }
        });

        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hapusJob();
            }
        });
    }

    private void hapusJob() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Yakin Ingin Hapus Data Ini ? ");
        alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Call<Value> call = ServiceGenerator.createService(MyServices.class)
                        .hapusJob(sIDUsers, sIdJob);
                call.enqueue(new Callback<Value>() {
                    @Override
                    public void onResponse(Call<Value> call, Response<Value> response) {
                        String value = response.body().value;
                        String message = response.body().message;
                        if (value.equals("1")){
                            Toast.makeText(DetailJob.this, message, Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        }else {
                            Toast.makeText(DetailJob.this, message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Value> call, Throwable t) {
                        Toast.makeText(DetailJob.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        alert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();

    }

    private void ubahJob() {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(DetailJob.this);
        progressDialog.setTitle("Loading...");
        progressDialog.show();
        File gambar = new File(imageUri.getPath());
        File compressImageFile = null;
        try {
            compressImageFile = new Compressor(this).compressToFile(gambar);
        } catch (IOException e) {
            e.printStackTrace();
        }

        MyServices myServices = ServiceGenerator.createService(MyServices.class);
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),compressImageFile);

        MultipartBody.Part Gambar = MultipartBody.Part.createFormData("uploaded_gambar", gambar.getName(), requestBody);
        RequestBody idUsers = RequestBody.create(MediaType.parse("text/plain"), sIDUsers);
        RequestBody idJob = RequestBody.create(MediaType.parse("text/plain"), sIdJob);
        RequestBody NamaJob = RequestBody.create(MediaType.parse("text/plain"), namaJob.getText().toString());
        RequestBody Posisi = RequestBody.create(MediaType.parse("text/plain"), posisi.getText().toString());
        RequestBody Lokasi = RequestBody.create(MediaType.parse("text/plain"), lokasi.getText().toString());
        RequestBody Deskripsi = RequestBody.create(MediaType.parse("text/plain"), deskripsi.getText().toString());
        RequestBody TglBerakhir = RequestBody.create(MediaType.parse("text/plain"), tglBerakhir.getText().toString());
        RequestBody JamMulai = RequestBody.create(MediaType.parse("text/plain"), jamMulai.getText().toString());
        RequestBody JamAkhir = RequestBody.create(MediaType.parse("text/plain"), jamAkhir.getText().toString());

        Call<Value> call = myServices
                .editJob(Gambar,idUsers,idJob,NamaJob, Posisi, Lokasi, Deskripsi,TglBerakhir, JamMulai, JamAkhir);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                progressDialog.dismiss();
                Toast.makeText(DetailJob.this, "Sukses", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(DetailJob.this, HalamanAdmin.class));
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(DetailJob.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void pickFormGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(DetailJob.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                    getString(R.string.permission_read_storage_rationale),
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT)
                    .setType("image/*")
                    .setAction(Intent.ACTION_PICK);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                String[] mimeTypes = {"image/jpeg", "image/png"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }

            final Intent chooserIntent = Intent.createChooser(intent, "Choose Image");
            startActivityForResult(chooserIntent, 1010);
        }
    }

    private void requestPermission(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(DetailJob.this, permission)) {
            showAlertDialog(getString(R.string.permission_title_rationale), rationale,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(DetailJob.this,
                                    new String[]{permission}, requestCode);
                        }
                    }, getString(R.string.label_ok), null, getString(R.string.label_cancel));
        } else {
            ActivityCompat.requestPermissions( DetailJob.this, new String[]{permission}, requestCode);
        }
    }
    protected void showAlertDialog(@Nullable String title, @Nullable String message,
                                   @Nullable DialogInterface.OnClickListener onPositiveButtonClickListener,
                                   @NonNull String positiveText,
                                   @Nullable DialogInterface.OnClickListener onNegativeButtonClickListener,
                                   @NonNull String negativeText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(DetailJob.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positiveText, onPositiveButtonClickListener);
        builder.setNegativeButton(negativeText, onNegativeButtonClickListener);
        mAlertDialog = builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        CropImage.ActivityResult result = CropImage.getActivityResult(intent);
        if (resultCode == RESULT_OK && requestCode == 1010){
            imageUri = result.getUri();
            gambar.setImageURI(imageUri);
            try {
                InputStream imageStream = getContentResolver().openInputStream(imageUri);
                Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                selectedImage = getResizedBitmap(selectedImage, 1024);// 400 is for example, replace with desired size

                gambar.setImageBitmap(selectedImage);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(imageUri, filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imagePath = cursor.getString(columnIndex);


                cursor.close();
                gambar.setVisibility(View.VISIBLE);

            } else {

                gambar.setVisibility(View.GONE);
                Toast.makeText(DetailJob.this, "Try again with another image", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
}

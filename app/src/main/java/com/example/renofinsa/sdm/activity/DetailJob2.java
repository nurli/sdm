package com.example.renofinsa.sdm.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.renofinsa.sdm.R;

public class DetailJob2 extends AppCompatActivity {

    Button btnApply, btnCancel;
    public String status;
    ImageView map;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_job2);

        btnApply = findViewById(R.id.btnApply);
        btnCancel = findViewById(R.id.btnCancel);
        map = findViewById(R.id.ivMaps);

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailJob2.this, Map2.class));
            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(DetailJob2.this);
                alert.setMessage("Anda Ingin Apply Job Ini ?");
                alert.setPositiveButton("Ya",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(DetailJob2.this, "Batas Lamaran Sudah Berakhir", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(DetailJob2.this, HalamanUtama.class));
                                status = "1";
                            }
                        });
                alert.setNegativeButton("Tidak",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(DetailJob2.this, "Kensel", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(DetailJob2.this, HalamanUtama.class));
                                status = "0";
                            }
                        });
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}

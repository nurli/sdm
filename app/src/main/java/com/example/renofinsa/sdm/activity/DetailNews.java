package com.example.renofinsa.sdm.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.example.renofinsa.sdm.R;

public class DetailNews extends AppCompatActivity {


    ImageView back, share;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_news);


        back = findViewById(R.id.ivBack);
        share = findViewById(R.id.ivShare);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String shareBody = "https://www.liputan6.com/global/read/3667808/7-pekerjaan-yang-akan-hilang-termakan-zaman-digantikan-robot";
                String shareSub = "7 Pekerjaan yang Akan Hilang Termakan Zaman, Digantikan Robot?";
                intent.putExtra(Intent.EXTRA_SUBJECT, shareSub);
                intent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(intent, "Share Using"));
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

}

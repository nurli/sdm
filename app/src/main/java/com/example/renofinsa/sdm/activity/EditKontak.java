package com.example.renofinsa.sdm.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.renofinsa.sdm.R;

public class EditKontak extends AppCompatActivity {

    EditText email,tlp;
    ImageView back,ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_kontak);

        back = findViewById(R.id.ivBack);
        ok = findViewById(R.id.ivOk);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(EditKontak.this, "Sukses", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        });

    }

}

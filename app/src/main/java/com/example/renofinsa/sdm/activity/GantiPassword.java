package com.example.renofinsa.sdm.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.model.Value;
import com.example.renofinsa.sdm.network.MyServices;
import com.example.renofinsa.sdm.network.ServiceGenerator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GantiPassword extends AppCompatActivity {

    ProgressBar pbGantiPass;
    Button btnGantiPass;
    EditText pass, rePass, email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ganti_password);

        pbGantiPass = findViewById(R.id.pbGantiPass);
        btnGantiPass = findViewById(R.id.btnGantiPass);
        pass = findViewById(R.id.etPassword);
        rePass = findViewById(R.id.etRePassword);
        email = findViewById(R.id.etEmail);

        pbGantiPass.setVisibility(View.GONE);

        btnGantiPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(email.getText().toString())){
                    email.setError("Masukan Email Anda");
                }else if (TextUtils.isEmpty(pass.getText().toString())){
                    pass.setError("Masukan Password Baru Anda");
                }else if (TextUtils.isEmpty(rePass.getText().toString())){
                    rePass.setError("Ulangi Password Baru Anda");
                }else if (!pass.getText().toString().equals(rePass.getText().toString())){
                    pass.setError("Password dan Re-Password Anda tidak sama!!");
                }
                else {
                    submit();
                }

            }
        });

    }

    private void submit() {
        Call<Value> call = ServiceGenerator.createService(MyServices.class)
                .ubahPassword(email.getText().toString(),
                        rePass.getText().toString());
        pbGantiPass.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                String value = response.body().value;
                String message = response.body().message;
                if (value.equals("1")){
                    Toast.makeText(GantiPassword.this, message, Toast.LENGTH_SHORT).show();
                    pbGantiPass.setVisibility(View.GONE);
                    startActivity(new Intent(GantiPassword.this, Login.class));
                    finish();
                    kosong();
                }else {
                    Toast.makeText(GantiPassword.this, message, Toast.LENGTH_SHORT).show();
                    pbGantiPass.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                Toast.makeText(GantiPassword.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                pbGantiPass.setVisibility(View.GONE);
            }
        });
    }

    private void kosong() {
        email.setText("");
        pass.setText("");
    }
}

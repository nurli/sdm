package com.example.renofinsa.sdm.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.adapter.MenuAdapter;
import com.example.renofinsa.sdm.util.Config;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

public class HalamanAdmin extends AppCompatActivity {

    DrawerLayout dlHalAdmin;
    Toolbar tbHalAdmin;
    FrameLayout flHalUtama;
    BottomNavigationViewEx bnveHalUtama;
    TabLayout tlHalAdmin;
    NavigationView nvHalAdmin;
    ActionBarDrawerToggle actionBarDrawerToggle;
    ViewPager vpHalamAdmin;
    ImageView ivMenu, photo;
    ActionBar actionBar;

    private final int[] tabIcons = {
            R.drawable.ic_business,
            R.drawable.ic_new,
            R.drawable.ic_profile
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman_admin);

        dlHalAdmin = findViewById(R.id.dlHalamanAdmin);
//        ivMenu = findViewById(R.id.ivMenu);
        tbHalAdmin = findViewById(R.id.tbHalAdmin);
//        flHalUtama = findViewById(R.id.flHalUtama);
//        bnveHalUtama = findViewById(R.id.bnveHalUtama);
        tlHalAdmin = findViewById(R.id.tabLayout);
        nvHalAdmin = findViewById(R.id.navHalAdmin);
        vpHalamAdmin = findViewById(R.id.vpHalAdmin);
        photo = findViewById(R.id.ivPhotoProfile);

        MenuAdapter adapter = new MenuAdapter(this, getSupportFragmentManager());
        vpHalamAdmin.setAdapter(adapter);
        tlHalAdmin.setupWithViewPager(vpHalamAdmin);

        setUpTabIcons();

        setSupportActionBar(tbHalAdmin);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        setUpDrawer();

        tbHalAdmin.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dlHalAdmin.openDrawer(Gravity.LEFT);
                    }
                }
        );
        nvHalAdmin.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.history:
                                dlHalAdmin.closeDrawers();
                                startActivity(new Intent(HalamanAdmin.this, Histori.class));
                                break;
                            case R.id.logout:
                                dlHalAdmin.closeDrawers();
                                logout();
                                break;
                        }
                        return false;
                    }
                }
        );
    }

    private void logout() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Anda Yakin Ingin Keluar ?");
        alert.setPositiveButton("Ya",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean(Config.LOGGEDIN_SHARED_PREF, false);
                        editor.putString(Config.ID, "");
                        editor.commit();
                        startActivity(new Intent(HalamanAdmin.this, Login.class));
                        finish();
                    }
                });
        alert.setNegativeButton("Tidak",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }

    private void setUpDrawer() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, dlHalAdmin,
                R.string.drawer_open, R.string.drawer_close){
            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        tbHalAdmin.setNavigationIcon(R.drawable.ic_menu);
        dlHalAdmin.setDrawerListener(actionBarDrawerToggle);
    }

    private void setUpTabIcons() {
        tlHalAdmin.getTabAt(0).setIcon(tabIcons[0]);
        tlHalAdmin.getTabAt(1).setIcon(tabIcons[1]);
        tlHalAdmin.getTabAt(2).setIcon(tabIcons[2]);
    }
    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.example.renofinsa.sdm.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TableLayout;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.adapter.MenuAdapter;
import com.example.renofinsa.sdm.fragment.Profile;
import com.example.renofinsa.sdm.fragment.Job;
import com.example.renofinsa.sdm.fragment.News;
import com.example.renofinsa.sdm.util.Config;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

public class HalamanUtama extends AppCompatActivity {

    DrawerLayout dlHalUtama;
    Toolbar tbHalUtama;
    FrameLayout flHalUtama;
    BottomNavigationViewEx bnveHalUtama;
    TabLayout tlHalUtama;
    NavigationView nvHalUtama;
    ActionBarDrawerToggle actionBarDrawerToggle;
    ViewPager vpHalamUtama;
    ImageView ivMenu, photo;
    ActionBar actionBar;

    private final int[] tabIcons = {
            R.drawable.ic_business,
            R.drawable.ic_new,
            R.drawable.ic_profile
    };

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman_utama);

        dlHalUtama = findViewById(R.id.dlHalUtama);
//        ivMenu = findViewById(R.id.ivMenu);
        tbHalUtama = findViewById(R.id.tbHalUtama);
//        flHalUtama = findViewById(R.id.flHalUtama);
//        bnveHalUtama = findViewById(R.id.bnveHalUtama);
        tlHalUtama = findViewById(R.id.tabLayout);
        nvHalUtama = findViewById(R.id.navHalUtama);
        vpHalamUtama = findViewById(R.id.vpHalUtama);
        photo = findViewById(R.id.ivPhotoProfile);




        MenuAdapter adapter = new MenuAdapter(this, getSupportFragmentManager());
        vpHalamUtama.setAdapter(adapter);
        tlHalUtama.setupWithViewPager(vpHalamUtama);

        setUpTabIcons();

        setSupportActionBar(tbHalUtama);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

//        tbHalUtama.setNavigationIcon(R.drawable.ic_menu);
        setUpDrawer();
//        loadFragment(new Job());

        tbHalUtama.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dlHalUtama.openDrawer(Gravity.LEFT);
                    }
                }
        );
        nvHalUtama.setNavigationItemSelectedListener(
                new OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.history:
                                dlHalUtama.closeDrawers();
                                startActivity(new Intent(HalamanUtama.this, Histori.class));
                                break;
                            case R.id.logout:
                                dlHalUtama.closeDrawers();
                                logout();
                                break;
                        }
                        return false;
                    }
                }
        );

//        bnveHalUtama.setOnNavigationItemSelectedListener(
//                new BottomNavigationView.OnNavigationItemSelectedListener() {
//                    @Override
//                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
//                        switch (menuItem.getItemId()){
//                            case R.id.news:
//                                loadFragment(new News());
//                                break;
//                            case R.id.profile:
//                                loadFragment(new Profile());
//                                break;
//                            case R.id.job:
//                                loadFragment(new Job());
//                                break;
//                        }
//                        return true;
//                    }
//                }
//        );

    }

    private void logout() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Anda Yakin Ingin Keluar ?");
        alert.setPositiveButton("Ya",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean(Config.LOGGEDIN_SHARED_PREF, false);
                        editor.putString(Config.ID, "");
                        editor.commit();
                        startActivity(new Intent(HalamanUtama.this, Login.class));
                        finish();
                    }
                });
        alert.setNegativeButton("Tidak",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }

    private void setUpTabIcons() {
        tlHalUtama.getTabAt(0).setIcon(tabIcons[0]);
        tlHalUtama.getTabAt(1).setIcon(tabIcons[1]);
        tlHalUtama.getTabAt(2).setIcon(tabIcons[2]);
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.vpHalUtama, fragment);
        fragmentTransaction.commit();
    }

    private void setUpDrawer() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, dlHalUtama,
                R.string.drawer_open, R.string.drawer_close){
            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        tbHalUtama.setNavigationIcon(R.drawable.ic_menu);
        dlHalUtama.setDrawerListener(actionBarDrawerToggle);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}


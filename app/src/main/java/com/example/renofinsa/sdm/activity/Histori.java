package com.example.renofinsa.sdm.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.adapter.HistoriAdapter;
import com.example.renofinsa.sdm.adapter.MenuAdapter;

public class Histori extends AppCompatActivity {

    ImageView tbHistori;
    TabLayout tlHistori;
    ViewPager vpHistori;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histori);

        tbHistori = findViewById(R.id.ivBack);
        tlHistori = findViewById(R.id.tlHistori);
        vpHistori = findViewById(R.id.vpHistori);

        tbHistori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                supportFinishAfterTransition();
            }
        });

//        setSupportActionBar(tbHistori);
//        getSupportActionBar().setTitle("Histori");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        HistoriAdapter adapter = new HistoriAdapter(this, getSupportFragmentManager());
        vpHistori.setAdapter(adapter);
        tlHistori.setupWithViewPager(vpHistori);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                supportFinishAfterTransition();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

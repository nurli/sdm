package com.example.renofinsa.sdm.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.model.Users;
import com.example.renofinsa.sdm.network.MyServices;
import com.example.renofinsa.sdm.network.ServiceGenerator;
import com.example.renofinsa.sdm.util.Config;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    TextView tvDaftar, tvLupaPass;
    ProgressBar pbLogin;
    Button btnLogin;
    EditText email, pass;
    private boolean loggedIn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tvDaftar = findViewById(R.id.tvDaftar);
        tvLupaPass = findViewById(R.id.tvLupaPassword);
        pbLogin = findViewById(R.id.pbLogin);
        btnLogin = findViewById(R.id.btnLogin);
        email = findViewById(R.id.etEmailLogin);
        pass = findViewById(R.id.etPasswordLogin);

        pbLogin.setVisibility(View.GONE);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(email.getText().toString())){
                    email.setError("Masukan Email Anda");
                }else if (TextUtils.isEmpty(pass.getText().toString())){
                    pass.setError("Masukan Password Anda");
                }else {
                    submitLogin();
                }

            }
        });

        tvDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, Daftar.class));
            }
        });

        tvLupaPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, LupaPassword.class));
            }
        });
    }

    private void submitLogin() {
        Call<Users> call = ServiceGenerator.createService(MyServices.class)
                .login(email.getText().toString(),
                        pass.getText().toString());
        pbLogin.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<Users>() {
            @Override
            public void onResponse(Call<Users> call, Response<Users> response) {
                String value = response.body().value;
                String id_users = response.body().id_users;
                String email = response.body().email_users;
                String pass = response.body().password;
                String level = response.body().level;
                String status = response.body().status_users;
                String biodata = response.body().biodata;

                if (value.equals("1")){
                    SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean(Config.LOGGEDIN_SHARED_PREF, true);
                    editor.putString(Config.ID, id_users);
                    editor.putString(Config.EMAIL_PREF, email);
                    editor.putString(Config.KEY_PASSWORD, pass);
                    editor.putString(Config.LEVEL_PREF, level);
                    editor.putString(Config.STATUS_PREF, status);
                    editor.putString(Config.BIODATA_PREF, biodata);

                    editor.commit();

                    if (level.equals("pelamar")){
                        if (status.equals("0")){
                            pbLogin.setVisibility(View.GONE);
                            Toast.makeText(Login.this, "Silahkan Verifikasi Email Anda", Toast.LENGTH_SHORT).show();
                        }else if (status.equals("1")){
                            if (biodata.equals("0")){
                                pbLogin.setVisibility(View.GONE);
                                Toast.makeText(Login.this, "Login Berhasil", Toast.LENGTH_SHORT).show();
                                Toast.makeText(Login.this, "Lengkapi Data Diri Anda", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(Login.this, Biodata.class));
                                kosong();
                            }else if (biodata.equals("1")){
                                pbLogin.setVisibility(View.VISIBLE);
                                Toast.makeText(Login.this, "Login Berhasil", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(Login.this, HalamanUtama.class));
                                kosong();
                            }
                        }
                    }else if (level.equals("admin")){
                        if (status.equals("0")){
                            pbLogin.setVisibility(View.GONE);
                            Toast.makeText(Login.this, "Silahkan Verifikasi Email Anda", Toast.LENGTH_SHORT).show();
                        }else if (status.equals("1")){
                            pbLogin.setVisibility(View.GONE);
                            Toast.makeText(Login.this, "Login Berhasil", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(Login.this, HalamanAdmin.class));
                            kosong();
                        }
                    }
                }else {
                    pbLogin.setVisibility(View.GONE);
                    Toast.makeText(Login.this, "Periksa Username dan Password Anda", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Users> call, Throwable t) {
                pbLogin.setVisibility(View.GONE);
                Toast.makeText(Login.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        loggedIn = sharedPreferences.getBoolean(Config.LOGGEDIN_SHARED_PREF, false);
        String level = sharedPreferences.getString(Config.LEVEL_PREF, "0");
        String biodata = sharedPreferences.getString(Config.BIODATA_PREF, "0");

        if (loggedIn){
            if (level.equals("pelamar") && biodata.equals("0")){
                Toast.makeText(this, "Lengkapi Data Diri Anda", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Login.this, Biodata.class));
            }else if (level.equals("pelamar") && biodata.equals("1")){
                startActivity(new Intent(Login.this, HalamanUtama.class));
            }else if (level.equals("admin")){
                startActivity(new Intent(Login.this, HalamanAdmin.class));
            }
        }
    }

    private void kosong() {
        email.setText("");
        pass.setText("");
    }
}

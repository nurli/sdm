package com.example.renofinsa.sdm.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.model.Value;
import com.example.renofinsa.sdm.network.MyServices;
import com.example.renofinsa.sdm.network.ServiceGenerator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LupaPassword extends AppCompatActivity {

    ProgressBar pbLupaPassword;
    Button btnLupaPass;
    EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lupa_password);

        pbLupaPassword = findViewById(R.id.pbLupaPassword);
        btnLupaPass = findViewById(R.id.btnSubmitLupaPass);
        email = findViewById(R.id.etEmail);

        pbLupaPassword.setVisibility(View.GONE);

        btnLupaPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(email.getText().toString())){
                    email.setError("Masukan Email Anda");
                }else {
                    submit();
                }

            }
        });

    }

    private void submit() {
        Call<Value> call =ServiceGenerator.createService(MyServices.class)
                .lupaPassword(email.getText().toString());
        pbLupaPassword.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                String value = response.body().value;
                String message = response.body().message;
                if (value.equals("1")){
                    Toast.makeText(LupaPassword.this, message, Toast.LENGTH_SHORT).show();
                    pbLupaPassword.setVisibility(View.GONE);
                    startActivity(new Intent(LupaPassword.this, VerifikasiKode.class));
                    kosong();
                }else {
                    Toast.makeText(LupaPassword.this, message, Toast.LENGTH_SHORT).show();
                    pbLupaPassword.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                Toast.makeText(LupaPassword.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                pbLupaPassword.setVisibility(View.GONE);
            }
        });
    }

    private void kosong() {
        email.setText("");
    }
}

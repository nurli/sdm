package com.example.renofinsa.sdm.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.renofinsa.sdm.R;
//import com.github.barteksc.pdfviewer.PDFView;
//import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
//import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
//import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;
//import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
//import com.nbsp.materialfilepicker.MaterialFilePicker;
//import com.nbsp.materialfilepicker.ui.FilePickerActivity;
//import com.shockwave.pdfium.PdfDocument;
//import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Pattern;

public class TambahBerkas extends AppCompatActivity implements OnPageChangeListener, OnLoadCompleteListener, OnPageErrorListener{


    ImageView back,ok;
    Button berkas;
    TextView namaFile;
    public static final int FILE_PICKER_REQUEST_CODE = 1;
    String pdfPath;
    String pdfFileName;
    private int pageNumber = 0;
    PDFView pdfView;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_berkas);


        back = findViewById(R.id.ivBack);
        ok = findViewById(R.id.ivOk);
        berkas = findViewById(R.id.btnBerkas);
        pdfView =findViewById(R.id.pdfViewProfile);
        namaFile = findViewById(R.id.tvNamaFile);



        berkas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lounchPicker();

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(TambahBerkas.this, "Sukses", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        });

    }

    private void lounchPicker() {
        new MaterialFilePicker()
                .withActivity(this)
                .withRequestCode(FILE_PICKER_REQUEST_CODE)
                .withHiddenFiles(true)
                .withFilter(Pattern.compile(".*\\.pdf$"))
                .withTitle("Select PDF File")
                .start();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FILE_PICKER_REQUEST_CODE && resultCode ==  RESULT_OK){
            String path = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);

            File filePdf = new File(path);
            displayFromFile(filePdf);
            if (path != null){
                Log.d("Path", path);
                pdfPath = path;
                Toast.makeText(this, "Picked File: " + path, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void displayFromFile(File filePdf) {
        Uri uri = Uri.fromFile(new File(filePdf.getAbsolutePath()));
        pdfFileName = getFileName(uri);
        namaFile.setText("Nama Berkas : "+pdfFileName);
        pdfView.fromFile(filePdf)
                .defaultPage(pageNumber)
                .onPageChange((ViewPager.OnPageChangeListener) this)
                .enableAnnotationRendering(true)
                .onLoad((OnLoadCompleteListener) this)
                .scrollHandle(new DefaultScrollHandle(this))
                .spacing(10)
                .onPageError((OnPageErrorListener) this)
                .load();
    }

    private String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")){
            Cursor cursor = this.getContentResolver().query(uri,
                    null,
                    null,
                    null,
                    null);
            try {
                if (cursor != null && cursor.moveToFirst()){
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            }finally {
                if (cursor != null){
                    cursor.close();
                }
            }
        }if (result == null){
            result = uri.getLastPathSegment();
        }
        return result;
    }


    @Override
    public void loadComplete(int nbPages) {
        com.shockwave.pdfium.PdfDocument.Meta meta = pdfView.getDocumentMeta();
        printBookmarksTree(pdfView.getTableOfContents(), "-");
    }

    private void printBookmarksTree(List<PdfDocument.Bookmark> tree, String s) {
        for (PdfDocument.Bookmark b : tree){
            if (b.hasChildren()){
                printBookmarksTree(b.getChildren(), s + "-");
            }
        }
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }

    @Override
    public void onPageError(int page, Throwable t) {

    }
}

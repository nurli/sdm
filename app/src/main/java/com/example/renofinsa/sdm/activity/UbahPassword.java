package com.example.renofinsa.sdm.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.fragment.Profile;

public class UbahPassword extends AppCompatActivity {

    ImageView back, ok;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_password);


        back =findViewById(R.id.ivBack);
        ok =findViewById(R.id.ivOk);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(UbahPassword.this, "Sukses", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        });


    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_ok, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case R.id.ok:
//                Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
//
//            case android.R.id.home:
//                onBackPressed();
//                supportFinishAfterTransition();
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }
}

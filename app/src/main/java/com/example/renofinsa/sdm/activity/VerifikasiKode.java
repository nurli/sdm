package com.example.renofinsa.sdm.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.model.Value;
import com.example.renofinsa.sdm.network.MyServices;
import com.example.renofinsa.sdm.network.ServiceGenerator;

import retrofit2.Callback;
import retrofit2.Response;

public class VerifikasiKode extends AppCompatActivity {

    ProgressBar pbVerifikasiKode;
    Button btnVerifikasiKode;
    EditText kode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifikasi_kode);

        pbVerifikasiKode = findViewById(R.id.pbVerifikasiKode);
        btnVerifikasiKode = findViewById(R.id.btnSubmitVerKode);
        kode = findViewById(R.id.etVerKode);

        pbVerifikasiKode.setVisibility(View.GONE);

        btnVerifikasiKode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(kode.getText().toString())){
                    kode.setError("Masukan Kode Verifikasi Anda");
                }else {
                    submit();
                }
            }
        });
    }

    private void submit() {
        retrofit2.Call<Value> call = ServiceGenerator.createService(MyServices.class)
                .checkVerKode(kode.getText().toString());
        pbVerifikasiKode.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(retrofit2.Call<Value> call, Response<Value> response) {
                String value = response.body().value;
                String message = response.body().message;
                if (value.equals("1")){
                    pbVerifikasiKode.setVisibility(View.GONE);
                    Toast.makeText(VerifikasiKode.this, message, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(VerifikasiKode.this, GantiPassword.class));
                    kosong();
                }else {
                    pbVerifikasiKode.setVisibility(View.GONE);
                    Toast.makeText(VerifikasiKode.this, message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<Value> call, Throwable t) {
                pbVerifikasiKode.setVisibility(View.GONE);
                Toast.makeText(VerifikasiKode.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void kosong() {
    }
}

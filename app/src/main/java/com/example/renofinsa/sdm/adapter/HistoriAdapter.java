package com.example.renofinsa.sdm.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.fragment.Proses;
import com.example.renofinsa.sdm.fragment.Setuju;
import com.example.renofinsa.sdm.fragment.TidakSetuju;

public class HistoriAdapter extends FragmentPagerAdapter {
    private Context mContext;
    public HistoriAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }


    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new Proses();
        } else if (position == 1){
            return new Setuju();
        } else if (position == 2){
            return new TidakSetuju();
        }
        else{
            return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return mContext.getString(R.string.proses);
            case 1:
                return mContext.getString(R.string.setuju);
            case 2:
                return mContext.getString(R.string.tdk_setuju);
                default:
        }
        return null;
    }
}

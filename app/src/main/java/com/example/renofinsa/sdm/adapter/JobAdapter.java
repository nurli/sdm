package com.example.renofinsa.sdm.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.activity.DetailJob;
import com.example.renofinsa.sdm.model.Job;
import com.example.renofinsa.sdm.util.PicassoCache;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.util.List;

public class JobAdapter extends RecyclerView.Adapter<JobAdapter.MyHolder> {
    com.example.renofinsa.sdm.fragment.Job context;
    List<Job> jobs;

    public JobAdapter(com.example.renofinsa.sdm.fragment.Job context, List<Job> jobs) {
        this.context = context;
        this.jobs = jobs;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.job_list, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyHolder myHolder,final int position) {
        myHolder.namaJob.setText(jobs.get(position).nama_job);
        myHolder.posisi.setText(jobs.get(position).posisi);
        myHolder.tglBerkahir.setText(jobs.get(position).tgl_berakhir);
        PicassoCache.getPicassoInstance(myHolder.itemView.getContext())
                .load("https://anurli.com/image/photo/" + jobs.get(position).uploaded_gambar)
                .into(myHolder.gambar);
        myHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Job job = jobs.get(position);
                Intent intent = new Intent(myHolder.itemView.getContext(), DetailJob.class);
                intent.putExtra("job", new GsonBuilder().create().toJson(job));
                myHolder.itemView.getContext().startActivity(intent);
            }
        });
        myHolder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String shareBody = "http://www.doyankuliner.com/article/detail/38/Bubur-Ayam-Monas-Ny.-Cirebon";
                String shareSub = "Bubur Ayam Monas Ny. Cirebon";
                intent.putExtra(Intent.EXTRA_SUBJECT, shareSub);
                intent.putExtra(Intent.EXTRA_TEXT, shareBody);
                myHolder.itemView.getContext().startActivity(Intent.createChooser(intent, "Share Using"));
            }
        });

    }

    @Override
    public int getItemCount() {
        return jobs.size();
    }


    class MyHolder extends RecyclerView.ViewHolder{
        ImageView gambar, share;
        TextView namaJob, posisi,tglBerkahir;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            share = itemView.findViewById(R.id.ivShare);
            gambar = itemView.findViewById(R.id.ivPhotoJob);
            namaJob = itemView.findViewById(R.id.tvJob);
            posisi = itemView.findViewById(R.id.tvPosisi);
            tglBerkahir = itemView.findViewById(R.id.tvtglBerakhir);


        }
    }
}

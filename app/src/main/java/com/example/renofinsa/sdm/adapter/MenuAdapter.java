package com.example.renofinsa.sdm.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.fragment.Job;
import com.example.renofinsa.sdm.fragment.News;
import com.example.renofinsa.sdm.fragment.Profile;

public class MenuAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public MenuAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new Job();
        } else if (position == 1){
            return new News();
        } else if (position == 2){
            return new Profile();
        }
        else{
            return null;
        }

    }

    @Override
    public int getCount() {
        return 3;
    }

//    @Nullable
//    @Override
//    public CharSequence getPageTitle(int position) {
//        switch (position){
//            case 0:
//                return mContext.getString(R.string.job);
//            case 1:
//                return mContext.getString(R.string.news);
//            case 2:
//                return mContext.getString(R.string.profile);
//                default:
//        }
//        return null;
//    }
}

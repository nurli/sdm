package com.example.renofinsa.sdm.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.activity.AddJob;
import com.example.renofinsa.sdm.activity.DetailJob;
import com.example.renofinsa.sdm.activity.DetailJob2;
import com.example.renofinsa.sdm.adapter.JobAdapter;
import com.example.renofinsa.sdm.network.MyServices;
import com.example.renofinsa.sdm.network.ServiceGenerator;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Job extends Fragment {

    FloatingActionButton addJob;
    SwipeRefreshLayout swipeRefreshLayout;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<com.example.renofinsa.sdm.model.Job> jobArrayList;
    JobAdapter jobAdapter;




    public Job() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_job, container, false);


        addJob = view.findViewById(R.id.fabAddJob);

        progressBar = view.findViewById(R.id.pbJob);
        swipeRefreshLayout = view.findViewById(R.id.swipeJob);
        recyclerView = view.findViewById(R.id.rvJob);

        progressBar.setVisibility(View.GONE);

        loadJobList();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                loadJobList();
                loadData();
            }
        });
        loadData();

        addJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AddJob.class));
            }
        });
        return view;
    }

    private void loadData() {
        Call<List<com.example.renofinsa.sdm.model.Job>> call = ServiceGenerator.createService(MyServices.class)
                .getJob();
        progressBar.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<List<com.example.renofinsa.sdm.model.Job>>() {
            @Override
            public void onResponse(Call<List<com.example.renofinsa.sdm.model.Job>> call, Response<List<com.example.renofinsa.sdm.model.Job>> response) {
                jobArrayList = new ArrayList<>(response.body());
                jobAdapter = new JobAdapter(Job.this, jobArrayList);
                recyclerView.setAdapter(jobAdapter);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<com.example.renofinsa.sdm.model.Job>> call, Throwable t) {
                Toast.makeText(getContext(), "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void loadJobList() {
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        jobArrayList = new ArrayList<>();
        jobAdapter = new JobAdapter(this, jobArrayList);
        recyclerView.setAdapter(jobAdapter);
    }

}

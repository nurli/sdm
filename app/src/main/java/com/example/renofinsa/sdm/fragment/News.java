package com.example.renofinsa.sdm.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.activity.AddNews;
import com.example.renofinsa.sdm.activity.DetailNews;
import com.example.renofinsa.sdm.activity.DetailNews2;

/**
 * A simple {@link Fragment} subclass.
 */
public class News extends Fragment {

    CardView cvNews1, cvNews2;
    FloatingActionButton addNews;
    ImageView share1, share2, share3, share4;


    public News() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);

        cvNews1 = view.findViewById(R.id.cvNews1);
        cvNews2 = view.findViewById(R.id.cvNews2);
        addNews = view.findViewById(R.id.fabAddNews);
        share1 = view.findViewById(R.id.ivShare);
        share2 = view.findViewById(R.id.ivShare2);
        share3 = view.findViewById(R.id.ivShare3);
        share4 = view.findViewById(R.id.ivShare4);

        share1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String shareBody = "https://www.liputan6.com/global/read/3667808/7-pekerjaan-yang-akan-hilang-termakan-zaman-digantikan-robot";
                String shareSub = "7 Pekerjaan yang Akan Hilang Termakan Zaman, Digantikan Robot?";
                intent.putExtra(Intent.EXTRA_SUBJECT, shareSub);
                intent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(intent, "Share Using"));
            }
        });
        share2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String shareBody = "https://www.liputan6.com/bisnis/read/3646747/kurang-tidur-akibat-pekerjaan-ini-solusinya";
                String shareSub = "Kurang Tidur Akibat Pekerjaan? Ini Solusinya";
                intent.putExtra(Intent.EXTRA_SUBJECT, shareSub);
                intent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(intent, "Share Using"));
            }
        });
        share3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String shareBody = "https://www.liputan6.com/lifestyle/read/3646565/yuk-dapatkan-pekerjaan-impian-dengan-mengubah-resume-anda";
                String shareSub = "Yuk, Dapatkan Pekerjaan Impian dengan Mengubah Resume Anda";
                intent.putExtra(Intent.EXTRA_SUBJECT, shareSub);
                intent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(intent, "Share Using"));
            }
        });
        share4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String shareBody = "https://www.liputan6.com/tekno/read/3647692/robot-bakal-gantikan-separuh-pekerjaan-manusia-pada-2025";
                String shareSub = "Robot Bakal Gantikan Separuh Pekerjaan Manusia pada 2025";
                intent.putExtra(Intent.EXTRA_SUBJECT, shareSub);
                intent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(intent, "Share Using"));
            }
        });


        addNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AddNews.class));
            }
        });

        cvNews2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), DetailNews2.class));
            }
        });

        cvNews1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), DetailNews.class));
            }
        });
        return view;
    }

}

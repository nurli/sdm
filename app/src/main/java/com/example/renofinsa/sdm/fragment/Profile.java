package com.example.renofinsa.sdm.fragment;


import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.renofinsa.sdm.R;
import com.example.renofinsa.sdm.activity.EditBahasa;
import com.example.renofinsa.sdm.activity.EditBerkas;
import com.example.renofinsa.sdm.activity.EditKontak;
import com.example.renofinsa.sdm.activity.EditPendidikan;
import com.example.renofinsa.sdm.activity.EditPengalaman;
import com.example.renofinsa.sdm.activity.SettingProfile;
import com.example.renofinsa.sdm.activity.TambahBahasa;
import com.example.renofinsa.sdm.activity.TambahBerkas;
import com.example.renofinsa.sdm.activity.TambahPendidikan;
import com.example.renofinsa.sdm.activity.TambahPengalaman;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class Profile extends Fragment {

    ImageView cvFile, setting, editBerkas, editPengalaman, editPendidikan, editBahasa, editKontak;
    ImageView tambahPengalaman, tambahPendidikan, tambahBahasa, tambahBerkas;
    CircleImageView foto;
    String pdfPath, pdfFileName;
    PDFView pdfView;
    public static final int FILE_PICKER_REQUEST_CODE = 1;
    protected static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    private int pageNumber = 0;
    private AlertDialog mAlertDialog;
    Uri uri;
    String imagePath;
    CardView cv1;

    public Profile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

//        cvFile = view.findViewById(R.id.ivCvFile);
//        pdfView = view.findViewById(R.id.pdfView);
        setting = view.findViewById(R.id.ivSetting);
        editBahasa = view.findViewById(R.id.ivEditBahasa);
        editBerkas = view.findViewById(R.id.ivEditBerkas);
        editPengalaman = view.findViewById(R.id.ivEditPengalaman);
        editPendidikan = view.findViewById(R.id.ivEditPendidikan);
        editKontak = view.findViewById(R.id.ivEditKontak);
        tambahBahasa = view.findViewById(R.id.ivTambahBahasa);
        tambahPendidikan = view.findViewById(R.id.ivTambahPendidikan);
        tambahPengalaman = view.findViewById(R.id.ivTambahPengalaman);
        tambahBerkas = view.findViewById(R.id.ivTambahBerkas);
        foto = view.findViewById(R.id.ivPhotoProfile);
        cv1 = view.findViewById(R.id.cvJob1);

//        cv1.setCardBackgroundColor(Color.TRANSPARENT);


        foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadImage();
            }
        });



        editKontak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), EditKontak.class));
            }
        });
        editPendidikan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), EditPendidikan.class));
            }
        });
        editPengalaman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), EditPengalaman.class));
            }
        });
        editBerkas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), EditBerkas.class));
            }
        });
        editBahasa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), EditBahasa.class));
            }
        });
        tambahPengalaman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), TambahPengalaman.class));
            }
        });
        tambahPendidikan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), TambahPendidikan.class));
            }
        });
        tambahBahasa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), TambahBahasa.class));
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), SettingProfile.class));
            }
        });
        tambahBerkas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), TambahBerkas.class));
            }
        });



//        cvFile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                lounchPicker();
//            }
//        });
        return view;

    }

    public void loadImage() {
        CropImage.activity()
                .start(getContext(), this);
    }

    private void getFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,
                    getString(R.string.permission_read_storage_rationale),
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        }else {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT)
                    .setType("image/*")
                    .setAction(Intent.ACTION_PICK);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
                String[] mimeTypes = {"image/jpg", "image/png"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }

            final Intent chooserIntent = Intent.createChooser(intent, "Choose Image");
            startActivityForResult(chooserIntent, 1010);
        }
    }
    private void requestPermissions(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                permission)){
            showAlertDialog(getString(R.string.permission_title_rationale),
                    rationale,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{permission}, requestCode);
                        }
                    }, getString(R.string.label_ok),
                    null,
                    getString(R.string.label_cancel));
        }else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{permission}, requestCode);
        }
    }
    private void showAlertDialog(@Nullable String title, @Nullable String message,
                                 @Nullable DialogInterface.OnClickListener onPositiveButtonClickListener,
                                 @NonNull String positiveText,
                                 @Nullable DialogInterface.OnClickListener onNegativeButtonClickListener,
                                 @NonNull String negativeText) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle(title);
        alert.setTitle(message);
        alert.setPositiveButton(positiveText, onPositiveButtonClickListener);
        alert.setNegativeButton(negativeText, onNegativeButtonClickListener);
        mAlertDialog = alert.show();
    }

    private void lounchPicker() {
        new MaterialFilePicker()
                .withActivity(getActivity())
                .withRequestCode(FILE_PICKER_REQUEST_CODE)
                .withHiddenFiles(true)
                .withFilter(Pattern.compile(".*\\.pdf$"))
                .withTitle("Select PDF File")
                .start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK){
                uri = result.getUri();
                foto.setImageURI(uri);
            }else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                Exception error = result.getError();
            }
        }
        if (resultCode == RESULT_OK && requestCode == 1010){
            Uri imageUri = data.getData();
            try {
                InputStream inputStream = getContext().getContentResolver().openInputStream(imageUri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                bitmap = getResizedBitmap(bitmap, 1024);

                foto.setImageBitmap(bitmap);
            }catch (FileNotFoundException e){
                e.printStackTrace();
            }

            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContext().getContentResolver().query(imageUri, filePathColumn, null,null,null);
            if (cursor != null){
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imagePath = cursor.getString(columnIndex);

                cursor.close();
                foto.setVisibility(View.VISIBLE);
            }else {
                foto.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Try Again With Another Image", Toast.LENGTH_SHORT).show();
            }
        }
//        if (requestCode == FILE_PICKER_REQUEST_CODE && resultCode ==  RESULT_OK){
//            String path = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
//
//            File filePdf = new File(path);
//            displayFromFile(filePdf);
//            if (path != null){
//                Log.d("Path", path);
//                pdfPath = path;
//                Toast.makeText(getContext(), "Picked File: " + path, Toast.LENGTH_SHORT).show();
//            }
//        }
    }
    private Bitmap getResizedBitmap(Bitmap image, int maxsize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitMapRatio = (float)width / (float)height;
        if (bitMapRatio > 1){
            width = maxsize;
            height = (int) (width / bitMapRatio);
        }else {
            height = maxsize;
            width = (int) (height / bitMapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void displayFromFile(File filePdf) {
        Uri uri = Uri.fromFile(new File(filePdf.getAbsolutePath()));
        pdfFileName = getFileName(uri);

        pdfView.fromFile(filePdf)
                .defaultPage(pageNumber)
                .onPageChange((OnPageChangeListener) getContext())
                .enableAnnotationRendering(true)
                .onLoad((OnLoadCompleteListener) getContext())
                .scrollHandle(new DefaultScrollHandle(getContext()))
                .spacing(10)
                .onPageError((OnPageErrorListener) getContext())
                .load();
    }

    private String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")){
            Cursor cursor = getContext().getContentResolver().query(uri,
                    null,
                    null,
                    null,
                    null);
            try {
                if (cursor != null && cursor.moveToFirst()){
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            }finally {
                if (cursor != null){
                    cursor.close();
                }
            }
        }if (result == null){
            result = uri.getLastPathSegment();
        }
        return result;
    }
}

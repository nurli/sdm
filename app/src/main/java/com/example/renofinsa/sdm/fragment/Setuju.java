package com.example.renofinsa.sdm.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.renofinsa.sdm.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Setuju extends Fragment {


    public Setuju() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_setuju, container, false);
    }

}

package com.example.renofinsa.sdm.model;

public class Job {
    public String id_job;
    public String id_users;
    public String nama_job;
    public String posisi;
    public String lokasi;
    public String deskripsi;
    public String tgl_berakhir;
    public String jam_mulai;
    public String jam_berakhir;
    public String uploaded_gambar;
}

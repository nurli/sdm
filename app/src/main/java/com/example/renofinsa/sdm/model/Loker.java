package com.example.renofinsa.sdm.model;

public class Loker {
    public String id;
    public String foto_loker;
    public String nama_mitra;
    public String jenis_pekerjaan;
    public String kota;
    public String tgl_posting;
    public String tanggung_jawab;
    public String kebutuhan;
    public String harapan;
    public String kemampuan;
    public String status;
}

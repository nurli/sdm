package com.example.renofinsa.sdm.model;

import com.google.gson.annotations.SerializedName;

public class Users {

    @SerializedName("id")
    public String id;
    @SerializedName("id_users")
    public String id_users;
    @SerializedName("biodata")
    public String biodata;
    @SerializedName("email_users")
    public String email_users;
    @SerializedName("password")
    public String password;
    @SerializedName("level")
    public String level;
    @SerializedName("status_users")
    public String status_users;
    @SerializedName("kode")
    public String kode;
    @SerializedName("value")
    public String value;
    @SerializedName("message")
    public String message;
    @SerializedName("photo")
    public String photo;
    @SerializedName("nama_lengkap")
    public String nama_lengkap;
    @SerializedName("nama_panggilan")
    public String nama_panggilan;
    @SerializedName("jenis_kelamin")
    public String jenis_kelamin;
    @SerializedName("tempat_lahir")
    public String tempat_lahir;
    @SerializedName("tanggal_lahir")
    public String tanggal_lahir;
    @SerializedName("umur")
    public String umur;
    @SerializedName("agama")
    public String agama;
    @SerializedName("no_ktp")
    public String no_ktp;
    @SerializedName("kota_ktp")
    public String kota_ktp;
    @SerializedName("no_bpjs")
    public String no_bpjs;
    @SerializedName("no_npwp")
    public String no_npwp;
    @SerializedName("nama_bank")
    public String nama_bank;
    @SerializedName("no_rek")
    public String no_rek;
    @SerializedName("alamat")
    public String alamat;
    @SerializedName("no_telp_rmh")
    public String no_telp_rmh;
    @SerializedName("no_handphone")
    public String no_handphone;
    @SerializedName("tinggi_badan")
    public String tinggi_badan;
    @SerializedName("berat_badan")
    public String berat_badan;
    @SerializedName("gol_darah")
    public String gol_darah;
    @SerializedName("status_data_diri")
    public String check_data_diri;
    @SerializedName("status_keluarga")
    public String check_keluarga;
    @SerializedName("status_formal")
    public String check_formal;
    @SerializedName("status_non_formal")
    public String check_non_formal;
    @SerializedName("status_pengalaman")
    public String check_pengalaman;
    @SerializedName("status_kemampuan")
    public String check_kemampuan;
    @SerializedName("status_psikotes")
    public String check_psikotes;
    @SerializedName("status_upload")
    public String check_upload;
    @SerializedName("alamat_perusahaan")
    public String alamat_perusahaan;
    @SerializedName("telepon_perusahaan")
    public String telepon_perusahaan;
    @SerializedName("email_perusahaan")
    public String email_perusahaan;
    @SerializedName("uploaded_logo")
    public String uploaded_logo;
    @SerializedName("website_perusahaan")
    public String website_perusahaan;
    @SerializedName("informasi_perusahaan")
    public String informasi_perusahaan;
    @SerializedName("cv")
    public String cv;
    @SerializedName("id_perusahaan")
    public String id_perusahaan;
    @SerializedName("id_permintaan_tko")
    public String id_permintaan_tko;
    @SerializedName("posisi")
    public String posisi;
    @SerializedName("jumlah")
    public String jumlah;
    @SerializedName("tgl_permintaan")
    public String tgl_permintaan;
    @SerializedName("tgl_mulai_kerja")
    public String tgl_mulai_kerja;
    @SerializedName("file")
    public String file;
    @SerializedName("status_permintaan_tko")
    public String status_permintaan_tko;
}

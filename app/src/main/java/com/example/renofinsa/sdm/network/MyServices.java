package com.example.renofinsa.sdm.network;

import com.example.renofinsa.sdm.model.Job;
import com.example.renofinsa.sdm.model.Loker;
import com.example.renofinsa.sdm.model.Users;
import com.example.renofinsa.sdm.model.Value;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface MyServices {

    @FormUrlEncoded
    @POST("daftarBMS3.php")
    Call<Value> daftarBMS(@Field("namaLengkap") String namaLengkap,
                          @Field("email") String email,
                          @Field("password") String password);

    @FormUrlEncoded
    @POST("login.php")
    Call<Users> login(@Field("email_users") String email_users,
                         @Field("password") String password);

    @FormUrlEncoded
    @POST("kirimKodeVerifikasi.php")
    Call<Value> lupaPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("checkKodeVerifikasi.php")
    Call<Value> checkVerKode(@Field("kode") String kode);

    @FormUrlEncoded
    @POST("ubahPassword.php")
    Call<Value> ubahPassword(@Field("email") String email,
                             @Field("password") String password);

    @FormUrlEncoded
    @POST("simpanDataDiri.php")
    Call<Value> simpanDataDiri(@Field("id") String id,
                               @Field("nama_panggilan") String nama_panggilan,
                               @Field("jenis_kelamin") String jenis_kelamin,
                               @Field("tempat_lahir") String tempat_lahir,
                               @Field("tanggal_lahir") String tanggal_lahir,
                               @Field("umur") String umur,
                               @Field("agama") String agama,
                               @Field("no_ktp") String no_ktp,
                               @Field("kota_ktp") String kota_ktp,
                               @Field("no_bpjs") String no_bpjs,
                               @Field("no_npwp") String no_npwp,
                               @Field("nama_bank") String nama_bank,
                               @Field("no_rek") String no_rek,
                               @Field("alamat") String alamat,
                               @Field("no_telepon_rumah") String no_telepon_rumah,
                               @Field("no_handphone") String no_handphone,
                               @Field("tinggi_badan") String tinggi_badan,
                               @Field("berat_badan") String berat_badan,
                               @Field("gol_darah") String gol_darah);


    @FormUrlEncoded
    @POST("simpanKeluarga.php")
    Call<Value> simpanKeluarga(@Field("id_users") String id_users,
                               @Field("status_kawin") String status_kawin,
                               @Field("nama_sutri") String nama_sutri,
                               @Field("tgl_lahir_sutri") String tgl_lahir_sutri,
                               @Field("tmpt_lahir_sutri") String tmpt_lahir_sutri,
                               @Field("status_keturunan") String status_keturunan,
                               @Field("nama_anak") String nama_anak,
                               @Field("jenis_kelamin") String jenis_kelamin,
                               @Field("tgl_lahir_anak") String tgl_lahir_anak,
                               @Field("pendidikan") String pendidikan,
                               @Field("keterangan") String keterangan);

    @Multipart
    @POST("simpanDetailCompany.php")
    Call<ResponseBody> simpanDetailCompany(@Part MultipartBody.Part file,
                                           @Part("alamat") RequestBody alamat,
                                           @Part("telepon") RequestBody telepon,
                                           @Part("email") RequestBody email,
                                           @Part("website") RequestBody website,
                                           @Part("informasi") RequestBody informasi,
                                           @Part("id_users") RequestBody id_users);

    @GET("tampilLoker.php")
    Call<List<Loker>> getLoker(@Query("id_loker") String id);

    @Multipart
    @POST("simpanDataDiri.php")
    Call<RequestBody> saveDataDiri(@Part RequestBody file,
                                   @Part("id") MultipartBody.Part id,
                                   @Part("nama_panggilan") RequestBody nama_pnggilan,
                                   @Part("jenis_kelamin") RequestBody jenis_kelamin,
                                   @Part("tempat_lahir") RequestBody tempat_lahir,
                                   @Part("tanggal_lahir") RequestBody tanggal_lahir,
                                   @Part("umur") RequestBody umur,
                                   @Part("agama") RequestBody agama,
                                   @Part("no_ktp") RequestBody no_ktp,
                                   @Part("kota_ktp") RequestBody kota_ktp,
                                   @Part("no_bpjs") RequestBody no_bpjs,
                                   @Part("no_bpjs") RequestBody no_npwp,
                                   @Part("nama_bank") RequestBody nama_bank,
                                   @Part("no_rek") RequestBody no_rek,
                                   @Part("alamat") RequestBody alamat,
                                   @Part("no_telepon_rumah") RequestBody no_telepon_rumah,
                                   @Part("no_handphone") RequestBody no_handphone,
                                   @Part("tinggi_badan") RequestBody tinggi_badan,
                                   @Part("berat_badan") RequestBody berat_badan,
                                   @Part("gol_darah") RequestBody gol_darah,
                                   @Part MultipartBody.Part pdf
    );

    @FormUrlEncoded
    @POST("daftarUsers.php")
    Call<Value> daftarUsers(@Field("namaLengkap") String namaLengkap,
                            @Field("email") String email,
                            @Field("password") String password,
                            @Field("level") String level);


    @POST("lihatUsersInstansi.php")
    Call<List<Users>> getUsers();

    @POST("lihatUsersInstansi.php")
    Call<List<Users>> getUsersInstansi();

    @FormUrlEncoded
    @POST("ubahUsersInstansi.php")
    Call<Value> ubahUsersInstansi(@Field("id_users") String id_users,
                                  @Field("namaLengkap") String namaLengkap,
                                  @Field("email") String email,
                                  @Field("level") String level);

    @FormUrlEncoded
    @POST("hapusUsersInstansi.php")
    Call<Value> hapusUsersInstansi(@Field("id_users") String id_users);

    @Multipart
    @POST("ubahPerusahaan.php")
    Call<ResponseBody> ubahProfilPerusahaan(@Part MultipartBody.Part logo,
                                            @Part("alamat_perusahaan") RequestBody alamat_perusahaan,
                                            @Part("telepon_perusahaan") RequestBody telepon_perusahaan,
                                            @Part("email_perusahaan") RequestBody email_perusahaan,
                                            @Part("website_perusahaan") RequestBody website_perusahaan,
                                            @Part("informasi_perusahaan") RequestBody informasi_perusahaan,
                                            @Part("id_users") RequestBody id_users,
                                            @Part("id_perusahaan") RequestBody id_perusahaan);

    @Multipart
    @POST("tambahPerusahaan.php")
    Call<ResponseBody> tambahProfilPerusahaan(@Part MultipartBody.Part logo,
                                              @Part("alamat_perusahaan") RequestBody alamat_perusahaan,
                                              @Part("telepon_perusahaan") RequestBody telepon_perusahaan,
                                              @Part("email_perusahaan") RequestBody email_perusahaan,
                                              @Part("website_perusahaan") RequestBody website_perusahaan,
                                              @Part("informasi_perusahaan") RequestBody informasi_perusahaan,
                                              @Part("id_users") RequestBody id_users,
                                              @Part("id_perusahaan") RequestBody id_perusahaan);

    @FormUrlEncoded
    @POST("lihatPerusahaan.php")
    Call<List<Users>> getPerusahaan(@Field("id_users") String id_users);

    @FormUrlEncoded
    @POST("tambahUsersInstansi.php")
    Call<Value> tambahUsersInstansi(@Field("namaLengkap") String namaLengkap,
                                    @Field("email") String email,
                                    @Field("password") String password,
                                    @Field("level") String level);


    @GET("lihatPermintaanTko.php")
    Call<List<Users>> lihatPermintaanTKO(@Query("id_users") String id_users);

    @Multipart
    @POST("tambahJob.php")
    Call<Value> tambahJob(@Part MultipartBody.Part uploaded_gambar,
                          @Part("id_users") RequestBody id_users,
                          @Part("nama_job") RequestBody nama_job,
                          @Part("posisi") RequestBody posisi,
                          @Part("lokasi") RequestBody lokasi,
                          @Part("deskripsi") RequestBody deskripsi,
                          @Part("tgl_berakhir") RequestBody tgl_berakhir,
                          @Part("jam_mulai") RequestBody jam_mulai,
                          @Part("jam_berakhir") RequestBody jam_berakhir);

    @GET("lihatJob.php")
    Call<List<Job>> getJob();

    @FormUrlEncoded
    @POST("ubahJob.php")
    Call<Value> ubahJob(@Field("id_users") String id_users,
                        @Field("id_job") String id_job,
                        @Field("nama_job") String nama_job,
                        @Field("posisi") String posisi,
                        @Field("lokasi") String lokasi,
                        @Field("deskripsi") String deskripsi,
                        @Field("jam_mulai") String jam_mulai,
                        @Field("jam_berakhir") String jam_berakhir);

    @Multipart
    @POST("tambahJob.php")
    Call<Value> editJob(@Part MultipartBody.Part uploaded_gambar,
                          @Part("id_users") RequestBody id_users,
                        @Part("id_job") RequestBody id_job,
                          @Part("nama_job") RequestBody nama_job,
                          @Part("posisi") RequestBody posisi,
                          @Part("lokasi") RequestBody lokasi,
                          @Part("deskripsi") RequestBody deskripsi,
                          @Part("tgl_berakhir") RequestBody tgl_berakhir,
                          @Part("jam_mulai") RequestBody jam_mulai,
                          @Part("jam_berakhir") RequestBody jam_berakhir);
    @FormUrlEncoded
    @POST("hapusJob.php")
    Call<Value> hapusJob(@Field("id_users") String id_users,
                         @Field("id_job") String id_job);

}

package com.example.renofinsa.sdm.util;

public class Config {

    //Keys for email and password as defined in our $_POST['key'] in login.php
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PASSWORD = "password";

    //If server response is equal to this that means login is successful
    public static final String LOGIN_SUCCESS = "success";

    //Keys for Sharedpreferences
    //This would be the name of our shared preferences
    public static final String SHARED_PREF_NAME = "myloginapp";

    //This would be used to store the email of current logged in user
    public static final String ID = "id";
    public static final String ID_PERUSAHAAN = "id_perusahaan";
    public static final String EMAIL_PREF = "email_users";
    public static final String NAMA_PREF = "nama";
    public static final String PHONE_PREF = "phone";
    public static final String LEVEL_PREF = "level";
    public static final String STATUS_PREF = "status";
    public static final String KODE_PREF = "kode";
    public static final String PHOTO_PREF = "photo";
    public static final String INSTANSI_PREF = "instansi";
    public static final String JENJANG_PREF = "jenjang";
    public static final String KOTA_PREF = "kota";
    public static final String EDI_PREF = "eid";
    public static final String TGL_PREF = "tgllahir";
    public static final String JK_PREF = "jk";
    public static final String POINT_PREF = "point";
    public static final String BIODATA_PREF = "biodata";
    public static final String NAMA_LENG_PREF = "nama_lengkap";
    public static final String NAMA_PANG_PREF = "nama_panggilan";
    public static final String TMPT_LAHIR_PREF = "tempat_lahir";
    public static final String TGL_LAHIR_PREF = "tanggal_lahir";
    public static final String UMUR_PREF = "umur";
    public static final String AGAMA_PREF = "agama";
    public static final String NO_KTP_PREF = "no_ktp";
    public static final String KOTA_KTP_PREF = "kota_ktp";
    public static final String NO_BPJS_PREF = "no_bpjs";
    public static final String NO_NPWP_PREF = "no_npwp";
    public static final String NAMA_BANK_PREF = "nama_bank";
    public static final String NO_REK_PREF = "no_rek";
    public static final String ALAMAT_PREF = "alamat";
    public static final String NO_TLP_RMH_PREF = "nomor_telepon_rumah";
    public static final String NO_HP_PREF = "nomor_handphone";
    public static final String TB_PREF = "tinggi_badan";
    public static final String BB_PREF = "berat_badan";
    public static final String GOL_PREF = "golongan_darah";
    public static final String CHECK_DATA_DIRI_PREF = "check_data_diri";
    public static final String CHECK_KELUARGA_PREF = "check_keluarga";
    public static final String CHECK_FORMAL_PREF = "check_formal";
    public static final String CHECK_NON_FORMAL_PREF = "check_non_formal";
    public static final String CHECK_PENGALAMAN_PREF = "check_pengalaman";
    public static final String CHECK_KEMAMPUAN_PREF = "check_kemampuan";
    public static final String CHECK_PSIKOTES_PREF = "check_pesikotes";
    public static final String CHECK_UPLOAD_PREF = "check_upload";
    public static final String ALAMAT_COMP = "alamat_comp";
    public static final String TELP_COMP = "telepon_comp";
    public static final String EMAIL_COMP = "email_comp";
    public static final String LOGO_COMP = "logo_perusahaan";
    public static final String PHOTO_COMP = "photo_comp";
    public static final String WEBSITE_COMP = "website_comp";
    public static final String INFORMASI_COMP = "informasi_comp";
    public static final String COVER_LOKER = "cover_loker";
    public static final String JENIS_PEKER_loker = "jensi_peker_loker";
    public static final String KOTA_LOKER = "informasi_loker";
    public static final String TGL_POST_LOKER = "tgl_post_loker";
    public static final String TANGGUNG_JAWAB_LOKER = "tanggung_jawab_loker";
    public static final String KEBUTUHAN_LOKER = "kebutuhan_loker";
    public static final String HARAPAN_LOKER = "harapan_loker";
    public static final String KEMAMPUAN_LOKER = "kemampuan_loker";
    public static final String STATUS_LOKER = "status_loker";
    public static final String NAMA_FILE_PDF = "nama_file_pdf";


    //We will use this to store the boolean in sharedpreference to track user is loggedin or not
    public static final String LOGGEDIN_SHARED_PREF = "loggedin";
}
